<?php
/**
 * Utilisations de pipelines par Colorbox
 *
 * @plugin     Colorbox
 * @copyright  2020
 * @author     Collectif
 * @licence    GNU/GPL
 * @package    SPIP\Colorbox\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


function colorbox_mediabox_config($config) {

	$config['_libs']['colorbox'] = [
		'nom' => 'Colorbox',
		'css' => [],
		'js' => [
			'colorbox/js/jquery.colorbox.js',
			'colorbox/js/colorbox.mediabox.js',
		]
	];

	if (empty($config['colorbox'])) {
		$config['colorbox'] = [];
	}

	$config['colorbox'] = array_merge(
		[
			'skin' => 'black-striped',
			'transition' => 'elastic',
			'speed' => '200',
			'maxWidth' => '90%',
			'maxHeight' => '90%',
			'minWidth' => '400px',
			'minHeight' => '',
			'slideshow_speed' => '2500',
			'opacite' => '0.9',
		]
		, $config['colorbox']
	);

	if (!empty($config['colorbox']['skin'])
		and $box_skin = $config['colorbox']['skin']) {
		$config['_libs']['colorbox']['css'][] = ($config['_public'] ? '' : 'prive/') . "colorbox/{$box_skin}/colorbox.css";
	}

	if ($config['_public']) {
		$config['box_type'] = 'colorbox';
	}
	return $config;
}
