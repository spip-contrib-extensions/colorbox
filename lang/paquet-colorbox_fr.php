<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'colorbox_description' => 'Ce plugin rétablit automatiquement Colorbox comme mediabox pour le site public, pour assurer la continuité de fonctionnement des sites qui avaient personalisé le fonctionnement de colorbox (styles ou comportement).',
	'colorbox_nom' => 'Colorbox',
	'colorbox_slogan' => 'Le retour de colorbox pour les nostalgiques de SPIP 3.2',
);
